export default
{
  "port": 3000,
  "dbFilePath": "./src/db/gameDB.db",
  "tokenLife": 60,
  "privatekey": "./config/secret.key",
  "serverKey": "./certificates/key.pem",
  "serverCert": "./certificates/cert.pem",
  "dimensionGame": 3
}
