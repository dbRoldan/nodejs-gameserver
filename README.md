# Server Game

### Description
Is a simple example of *API Restfull*, in a server side, this contains some services for *each users:*
* **LogIn**
* **SignIn**
* **Game history and game scores**
*Within the functionality of the server side will take into account the use of* **Websockets**.

Too this project allows the visualization of information of the community of registered users.

The game in the server is ***Triqui Game***, as is a basic game allows to see different functions in a connection process of sockets; the objective of the game is to align the player's repective figures in the X or Y axis , while the opposing player makes the process more difficult.
![Triqui Game Image](docs/game.png "Triqui Game Diagram")


### Architecture
#### DB Diagram
![Relationship Diagram Image](docs/relationship_diagram.png "Relationship Diagram")

### Tools
* NodeJS
* SQLite
* socket.io

### Developed by
dbgroldan

### License
GPL-3.0 or later.
