import sqlite3 from 'sqlite3';
//const sqlite3 = require("sqlite3").verbose();

let db = null;

class Connection{
  constructor(dbFilePath){
    if (!db){
      db = new sqlite3.Database(dbFilePath, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
        if (!err) {
          console.log("DB: Connected to database");
          this.createTables(db);
        } else {
          console.log("DB: Error ", err);
        }
      });
    }
    return db;
  }

  createTables(db){
    db.parallelize(() => {
      db.run("CREATE TABLE IF NOT EXISTS users(u_id TEXT PRIMARY KEY, u_name TEXT, u_lastname TEXT, u_nickname TEXT, u_email TEXT, u_image TEXT, u_password TEXT)");
      db.run("CREATE TABLE IF NOT EXISTS matches(mth_id INTEGER PRIMARY KEY AUTOINCREMENT, mth_score REAL, mth_date TEXT, mth_state INTEGER)");
    });
    db.serialize(() => {
      db.run("CREATE TABLE IF NOT EXISTS games(g_id INTEGER PRIMARY KEY, ug_id TEXT, mthg_id INTEGER, FOREIGN KEY(ug_id) REFERENCES users(u_id), FOREIGN KEY(mthg_id) REFERENCES matches(mth_id))")
    });
    console.log("DB: Schema created");
  }
}

export default Connection;
//module.exports = Connection;
