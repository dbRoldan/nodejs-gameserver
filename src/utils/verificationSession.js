import jwt from 'jsonwebtoken';
import fs from 'fs';
import config from '../../config/config.js';
import userController from '../controllers/user.controller.js';

// const jwt = require('jsonwebtoken');
// const fs   = require('fs');
// const config = require('../../config/config');
// const userController = require('../controllers/user.controller');
const userControl = new userController();

export default async function verifyToken(tokenExist) {
  if (tokenExist.startsWith('Bearer')){
    tokenExist = tokenExist.split(' ')[1];
  }
  let privateKEY  = fs.readFileSync(config.privatekey, 'utf8');
  var decoded = jwt.verify(tokenExist, privateKEY);
  let userExist = await userControl.findUserById(decoded.id);
  if (userExist){
    if(decoded.email === userExist.u_email && decoded.password === userExist.u_password){
      return userExist;
    }
  }
}

// module.exports = {
//   verifyToken
// };
