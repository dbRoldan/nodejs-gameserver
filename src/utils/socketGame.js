import triquiGame from './triquiGame.js';
import config from '../../config/config.js';
const dimensionBoard = config.dimensionGame;
let controlGame = new triquiGame(dimensionBoard);

export default function socketGame(io){
  // TODO: Verification token
  // TODO: Redis view
  return io.of('/users/game').on('connection', (socket) => {
    console.log('Server: New Connection', socket.id);

    socket.on('greeting', (msg) => {
      console.log(msg);
    });

    socket.emit('greeting', {message: "Hi User, Triqui game started!"});

    for (let i = 0; i < (dimensionBoard * 2); i++) {
      let move = null;
      socket.on('set-piece', (dataPiece) => {
        move = dataPiece;
        console.log('Move piece:', move);
        if (move){
          if (controlGame.setPiece(move.player, move.posX, move.posY)) {
            socket.emit('get-piece', {message: "You moved", piece: move});
            let board = controlGame.getBoard();
            console.log('Board: ', board);
            socket.emit('look-board', {message: "Board: Game State", board: board});
            if (!controlGame.verifyStateGame(board, move.player)) {
              socket.emit('end-game', {message: "You Win, Congratulations " + move.player, board: board});
              console.log("User Win");
              console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            }
          } else {
            socket.emit('get-piece', {message: "You could not move piece", piece: move});
          }
        }
      });

    }

    //io.sockets.emit('msg') //message to al users
    socket.on('disconnect', () => {
      let msg = {message:'Server: User disconnected'};
      console.log(msg);
      io.emit('disconnect', msg);
    });
  });
}

// export { func1, func2 };
