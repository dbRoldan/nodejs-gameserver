class triquiGame {
  constructor(dimension) {
    this.matrixBoard = [];
    console.log('dimesion', dimension);
      for (let i = 0; i < dimension; i++)
        this.matrixBoard.push(Array.apply(null, Array(dimension)).map(Number.prototype.valueOf,0));
  }

  setPiece(player, posX, posY){
    if (this.matrixBoard[posX][posY] == 0){
        this.matrixBoard[posX][posY] = player;
        return true;
    } else {
      return false;
    }
  }

  getBoard(){
    return this.matrixBoard;
  }

  verifyStateGame(matrixBoard, player){
    let wAxis = true;
    for (let j = 0; j < this.matrixBoard.length; j++) {
      for (let i = 0; i < this.matrixBoard[j].length; i++) {
        if (this.matrixBoard[i][j] != player)
          wAxis = false;
      }
      if (wAxis)
        return true;    // |||
      wAxis = true;
    }
    for (let i = 0; i < this.matrixBoard.length; i++) {
      for (let j = 0; j < this.matrixBoard[i].length; j++) {
        if (this.matrixBoard[i][j] != player)
          wAxis = false;
      }
      if (wAxis)
        return true;    // ---
      wAxis = true;
    }

    for (let i = 0; i < this.matrixBoard.length; i++) {
      if (this.matrixBoard[i][i] != player)
        wAxis = false;
    }

    for (let i = 0; i < this.matrixBoard.length; i++) {
      if (this.matrixBoard[(this.matrixBoard.length-1)-i][i] != player)
        wAxis = false;
    }

    if (wAxis)
      return true;    // ///
    wAxis = true;
    return false;
  }

}

export default triquiGame;
