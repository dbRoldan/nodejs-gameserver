import express from 'express';

import matchController from '../controllers/match.controller.js';
const matchControl = new matchController();

const router = express.Router();

router.get('/', async(req, res) => {
  let matchesResult = await matchControl.findAllMatchesOrganized();
  res.status(200).send(matchesResult);
});

router.get('/:id', async (req, res, next) => {
  let id = req.params.id;
  if (!id) {
    console.log('Server: Invalid Data');
    next({code: 400, error : "Invalid Data"});
  } else {
    let matchResult = await matchControl.findMatchById(id);
    if (matchResult){
      res.status(200).send(matchResult);
    } else {
      console.log("Server: Unauthorized Match");
      next({code: 401 , error: "Unauthorized Match"});
    }
  }
});



export default router;
