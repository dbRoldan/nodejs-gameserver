import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import fs from 'fs';
import config from '../../config/config.js';
import userController from '../controllers/user.controller.js';
import matchController from '../controllers/match.controller.js';
import verifyToken from '../utils/verificationSession.js';
// ** const defineUri = "/users";

const userControl = new userController();
const matchControl = new matchController();
const router = express.Router();



router.get('/', async (req, res, next) => {
  let tokenExist = req.headers.authorization;
  if (tokenExist){
    let userExist = await verifyToken(tokenExist);
    if (userExist){
      res.status(200).send(userExist);
    } else {
      console.log("Server: Unauthorized User");
      next({code: 401 , error: "Unauthorized User"});
    }
  } else {
    res.send({message: "Welcome", statusLogin: false});
  }
});


router.post('/signin', async (req, res, next) => {
  try {
    let user = req.body;
    let { id, name, lastname, nickname, email, image, password } = user;
    if (!!id && !!name && !!lastname &&  !!nickname && !!password && !!email && !!image ) {
      let userExist = await userControl.findUserById(id);
      if (!userExist) {
        let result = await userControl.addUser(user);
        console.log(result);
        res.status(200).send(result);
      } else {
        console.log('Server: Forbidden, user already exist');
        next({code: 403, error: "Forbidden, user already exist"});
      }
    } else {
      console.log('Server: Invalid Data');
      next({code: 400, error : "Invalid Data"});
    }
  } catch (err) {
    next(err);
  }
});

router.post('/login', async (req, res, next) => {   //login with id, because other data could be repeated
  let id = req.body.id;
  let password = req.body.password;
  if (id && password) {
    let user = await userControl.findUserById(id);
    if (user){
      let match = await bcrypt.compare(password, user.u_password);
      if(match) {
        let payload = {
          "id": user.u_id,
          "email": user.u_email,
          "password": user.u_password
        };
        // Replace secret key, TSL is an option, key.key algorithm: 'RS256',
        let privateKEY  = fs.readFileSync(config.privatekey, 'utf8');
        let token = jwt.sign(payload, privateKEY, { expiresIn: config.tokenLife });
        let response = {
          "statusLogin": true,
          "access_token": token
        };
        console.log(`Server: ${user.u_name} is logged! `);
        res.status(200).json({user, response});
        //res.status(200).json(response);
      } else {
        console.log("Server: Unauthorized User");
        next({code: 401 , error: "Unauthorized User"});
      }
    } else {
      console.log("Server: User Not Found");
      next({code: 404, error: "User Not Found"});
    }
  } else {
    console.log('Server: Invalid Data');
    next({code: 400, error : "Invalid Data"});
  }
});

router.get('/profile/:id', async (req, res, next) => {
  let id = req.params.id;
  if (!id) {
    console.log('Server: Invalid Data');
    next({code: 400, error : "Invalid Data"});
  } else {
    let tokenExist = req.headers.authorization;
    if (tokenExist){
      let userExist = await verifyToken(tokenExist);
      if (userExist){
        res.status(200).send(userExist);
      } else {
        console.log("Server: Unauthorized User");
        next({code: 401 , error: "Unauthorized User"});
      }
    } else {
      console.log("Server: User Not Found");
      next({code: 404, error: "User Not Found"});
    }
  }
});

router.put('/update/:id', async (req, res, next) => {
  let id = req.params.id;
  if (!id) {
    console.log("Server: Invalid Data");
    next ({ code: 400, error: "Invalid Data" });
  } else {
    let userExist = await userControl.findUserById(id);
    if (userExist) {
      try {
        let userData = req.body;
        let {name, lastname, nickname, email, image, password, initialPass, confirmationPass } = userData;
        if (id && name && lastname &&  nickname && password && email && image && initialPass && confirmationPass) {
          let result = await userControl.updateUser(userData);
          console.log(result);
          res.status(200).send(result);
        } else {
          console.log('Server: Invalid Data');
          next({code: 400, error : "Invalid Data"});
        }
      } catch (err) {
        next(err);
      }
    } else {
      console.log("Server: User Not Found");
      next({code: 404, error: "User Not Found"});
    }
  }
});


router.get('/scores/:id', async (req, res, next) => {
  let id = req.params.id;
  if (!id) {
    console.log("Server: Invalid Data");
    next ({ code: 400, error: "Invalid Data" });
  } else {
    let userExist = await userControl.findUserById(id);
    if (userExist) {
      let dataMatches = await matchControl.findMatchesByUserId(id);
      let result = { user:userExist, matches:dataMatches };
      console.log(result);
      res.status(200).send(result);
    } else {
      console.log("Server: User data not found");
      next ({ code: 404, error: "User data not found" });
    }
  }
});

router.get('/delete/:id', async (req, res, next) => {
  let id = req.params.id;
  if (!id) {
    console.log("Server: Invalid Data");
    next ({ code: 400, error: "Invalid Data" });
  } else {
    let userExist = await userControl.findUserById(id);
    if (userExist) {
      let result = await userControl.deleteUser(id);
      console.log(result);
      res.status(200).send(result);
    } else {
      console.log("Server: User data not found");
      next ({ code: 404, error: "User data not found" });
    }
  }
});

// ** router.params = [defineUri];
export default router;
// module.exports = router;
