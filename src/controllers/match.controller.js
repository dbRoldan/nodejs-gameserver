import config from '../../config/config.js';
import Connection from '../db/connection.js';
import triquiGame from '../utils/triquiGame.js';
// const socket = require ('..utils/socket.js').socket;

let db = null;

class MatchControl {
  constructor() {
    if (!db){
      db = new Connection(config.dbFilePath);
    }
  }

  addMatch(match, userId, gameId){
    return new Promise((resolve, reject) => {
      let matchData = Object.keys(match).map(key => match[key]);
      db.serialize(() => {
        db.run("INSERT INTO matches VALUES(NULL, ?, ?, ?)", matchData, async function (err) { //If define like arrow function, not function
          if (err) {
            reject('DB Error ', err.message);
          } else {
            db.serialize(() => {
              db.run("INSERT INTO games VALUES(?, ?, ?)", [gameId, userId, this.lastID],(err) => {
                if (err)
                  reject('DB Error ', err.message);
              });
            });
            resolve(this.lastID);
          }
        });
      });
    });
  }

  findMatchById(id){
    return new Promise((resolve, reject) => {
      let query = 'SELECT * FROM matches WHERE mth_id = ?';
      db.serialize(() => {
        db.get(query, id, (err, row) => {
          if (err) {
            reject('DB Error: ', err.message);
          } else {
            resolve(row);
          }
        });
      });
    });
  }

  findAllMatchesOrganized(){
    return new Promise((resolve, reject) => {
      let query = 'SELECT * FROM matches ORDER BY mth_score DESC';
      db.serialize(() => {
        db.all(query, (err, rows) => {
          if (err) {
            reject('DB Error: ', err.message);
          } else {
            resolve(rows);
          }
        });
      });
    });
  }

  findMatchesByUserId(userId){
    return new Promise((resolve, reject) =>{
      db.serialize(() => {
        let query = `SELECT * FROM matches
                  INNER JOIN games ON games.mthg_id = matches.mthg_id
                  INNER JOIN users ON users.u_id = games.ug_id
                  WHERE users.u_id = ?`;
        db.all(query, [userId], (err, rows) => {
          if (err) {
            reject('DB Error: ', err.message);
          } else {
            resolve(rows);
          }
        });
      });
    });
  }

  deleteMatch(id){
    return new Promise((resolve, reject) => {
      let query = 'DELETE FROM matches WHERE mth_id = ?';
      db.serialize(() => {
        db.get(query, id, (err) => {
          if (err) {
            reject ('DB Error: ', err.message);
          } else {
            resolve({ message: "Match deleted"});
          }
        });
      });
    });
  }

}
export default MatchControl;
