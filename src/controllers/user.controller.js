import bcrypt from 'bcrypt';
import config from '../../config/config.js';
import Connection from '../db/connection.js';

let db = null;

class UserControl{
  constructor(){
    if (!db){
      db = new Connection(config.dbFilePath);
    }
  }

  addUser(user) {
    return new Promise( async (resolve, reject) => {
      let userData = Object.keys(user).map(key => user[key]);
      let saltRounds = 10;
      await bcrypt.hash(userData[userData.length - 1], saltRounds).then((hash) => {
        userData[userData.length - 1] = hash;
      });
      
      db.serialize(() => {
        db.run("INSERT INTO users VALUES(?,?,?,?,?,?,?)", userData, async (err) => {
          if (err) {
            reject('DB Error ', err.message);
          } else {
            resolve( await this.findUserById(userData[0]) );
          }
        });
      });
    });
  }

  findUserById(id) {
    return new Promise((resolve, reject) => {
      let query = 'SELECT * FROM users WHERE u_id = ?';
      db.serialize(() => {
        db.get(query, id, (err, row) => {
          if (err) {
            reject('DB Error: ', err.message);
          } else {
            resolve(row);
          }
        });
      });
    });
  }

  updateUser(user) {
    return new Promise( async (resolve, reject) => {
      let userData = Object.keys(user).map(key => user[key]);
      let confPass = userData.pop();
      let iniPass = userData.pop();
      if (confPass === iniPass){
        let userId = userData.shift();
        userData.push(userId);
        console.log(userData);
        let query = `UPDATE users
                      SET u_name = ?,
                      u_lastname = ?,
                      u_nickname = ?,
                      u_email = ?,
                      u_image = ?,
                      u_password = ?
                      WHERE u_id = ?`;
        db.serialize(() => {
          db.run(query, userData, async (err) => {
            if (!err)
              resolve (await this.findUserById(userId));
            else
              reject('DB Error ', err.message);
            });
        });
      }
    });
  }

  deleteUser(id) {
    return new Promise((resolve, reject) => {
      let query = 'DELETE FROM users WHERE u_id = ?';
      db.serialize(() => {
        db.get(query, id, (err) => {
          if (err) {
            reject ('DB Error: ', err.message);
          } else {
            resolve({ message: "User deleted", statusLogin: true});
          }
        });
      });
    });
  }
}

export default UserControl;
// module.exports = UserControl;
