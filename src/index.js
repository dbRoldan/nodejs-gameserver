// import https from "https";
import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';
import config from '../config/config.js';
import userRoutes from './routes/user.routes.js';
import matchRoutes from './routes/match.routes.js';

import socketIO from 'socket.io';
import socketGame from './utils/socketGame.js';
//import { func1, func2 } from './utils/socketGame.js';

const port = config.port;
const app = express();


app.use(bodyParser.json());
app.use('/users', userRoutes);
app.use('/matches', matchRoutes);
// ** app.use(userRoutes.params[0], userRoutes);


const server = app.listen(port, () => {
 console.log(`Server: Server app listening on port ${port}!`);
});

let wsGame = socketGame(socketIO(server));


//HTTPS
// // app.set('port', process.env.PORT || 3001); Best for de verification port connection
// app.set('port', port || 3001);
//
// const optionsHTTPS = {
//   key: fs.readFileSync(config.serverKey),
//   cert: fs.readFileSync(config.serverCert)
// };
//
// https.createServer(optionsHTTPS, app).listen(app.get('port'), () => {
//   console.log(`Server: App listening on port ${app.get('port')}! Go to https://localhost:${app.get('port')}/`)
// });
